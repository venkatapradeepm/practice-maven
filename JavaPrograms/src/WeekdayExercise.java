import java.util.Scanner;

public class WeekdayExercise {
	public static void main(String[] args) {
		System.out.println("Weekday 1: Sunday");
		System.out.println("Weekday 2: Monday");
		System.out.println("Weekday 3: Tuesday");
		System.out.println("Weekday 4: Wednesday");
		System.out.println("Weekday 5: Thursday");
		System.out.println("Weekday 6: Friday");
		System.out.println("Weekday 7: Saturday");
		System.out.println("Weekday 0: Saturday");
	
		Scanner S = new Scanner (System.in);
		System.out.println("Enter the weekday number");
		int number = S.nextInt();
		
		if(number == 1) {
						System.out.println("Today is a Sunday");
			}
		else if(number == 2) {
			System.out.println("Today is a Monday");
			}
		else if(number == 3) {
			System.out.println("Today is a Tuesday");
			}
		else if(number == 4) {
			System.out.println("Today is a wednesday");
			}
		else if(number == 5) {
			System.out.println("Today is a Thursday");
			}
		else if(number == 6) {
			System.out.println("Today is a Friday");
			}
		else if(number == 7) {
			System.out.println("Today is a Saturday");
			}
		else if(number == 0) {
			
			System.out.println("Today is a Saturday");
			}
		else {
			System.out.println("Weekday " + number + ": Error ");
		}
		
	}
}
 
