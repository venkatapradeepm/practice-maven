import java.util.Scanner;

public class ElseIf {
public static void main (String[] args) {
	Scanner S = new Scanner (System.in);
	System.out.println("Hey What's your name? Sorry, I keep forgetting.");
	String name = S.nextLine();
	System.out.println("OK," + name + ", How old are you ?");
	int age = S.nextInt();
	
	if (age < 16) {
		System.out.println(" You Can't Drive ," + name);	
	}
	
	else if(age >15 && age <18) {
		System.out.println(" You Cant Drive but you can't vote ," + name);	
	}
	else if(age >17 && age <25) {
		System.out.println(" You Can vote but you can't rent a car, " + name);	
	}
	else if(age >24) {
		System.out.println(" You Can do pretty much anything, " + name);	
	}
}
}
