
public class ObjTypeConversion {
public static void main(String[] args) {
	
	//Converting a prim to obj type
	int number =100;
	int number2 = 10002;
	Integer obj = Integer.valueOf(number); // Converting int to integer explicitly
	Integer objNum = number2;  // inplicit type conversion or autoboxing
	System.out.println(number +" "+ obj);
	System.out.println(number +" "+ objNum);
	
	//Converting obj type to primitive type
	
	Integer obj2 = new Integer(100);
	Integer intObj2= new Integer(500);
	int num = obj2.intValue(); // explicit -objct int to primitive int
	int num2= intObj2; // implicit-- autounboxing
			System.out.println(num+ "  " + obj2);
			System.out.println(num+ "  " + obj2);
                    }
//Converting obj type to primitive type float

     float num1 = 10000;
     Float obj2 = Float.valueOf(num1);


}
